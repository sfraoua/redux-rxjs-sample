import 'rxjs';
import React from "react"
import ReactDOM from "react-dom"
import { Provider } from 'react-redux';

import { Router, Route, IndexRoute, hashHistory } from "react-router";

import configureStore from "./src/configureStore"
import Layout from "./src/components/Layout"
import Index from "./src/components/pages/Index"
import _  from 'lodash'

const app = document.getElementById('app');

ReactDOM.render(
<Provider store={configureStore()}>
    <Router history={hashHistory}>
    <Route path="/" component={Layout}>
    <IndexRoute component={Index}></IndexRoute>
    </Route>
    </Router>
    </Provider>,
    app);