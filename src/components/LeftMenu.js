import React from "react"
import { IndexLink, Link } from "react-router";

export default class LeftMenu extends React.Component {

    render() {
        return <div class="col-md-2 bg-faded sidebar">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <IndexLink class="nav-link" to="/" >Index</IndexLink>
                </li>
                <li class="nav-item">
                    <IndexLink class="nav-link" to="/menu2" >Menu 2</IndexLink>
                </li>
                <li class="nav-item">
                    <Link class="nav-link" to="/menu3" >Menu 3</Link>
                </li>
            </ul>
        </div>
    }
}

