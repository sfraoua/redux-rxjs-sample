import React from "react"
import {connect} from "react-redux";
import { Link } from "react-router";


@connect((store) => {
    return {
        header: store.header,
    };
})
export default class Title extends React.Component {

    componentWillMount() {
    }

    render() {
        const mappedFlash = this.props.header.flashes.map((flash, i ) =>
            <li class="breadcrumb-item" key={i}>{flash.message}</li>);

        return <div>
            <h1>{this.props.header.title}</h1>

            <ol class="breadcrumb">
                {mappedFlash}
            </ol>
            {
                this.props.header.alert != null &&
                <div  class={"alert alert-"+this.props.header.alert.type+" alert-dismissible fade show"} role="alert">
                    {this.props.header.alert.message}
                </div>
            }
        </div>
    }
}

