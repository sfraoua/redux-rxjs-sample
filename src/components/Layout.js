import React from "react"
import { IndexLink, Link } from "react-router";

import TopBar from "./TopBar"
import LeftMenu from "./LeftMenu"
import Header from "./Header"

export default class Layout extends React.Component {

    render() {
        return <div class="container-fluid">
            <TopBar />
            <div class="row">
                <LeftMenu />
                <div class="col-md-10 offset-md-2 pt-3">
                    <Header />
                    {this.props.children}
                </div>
            </div>
        </div>
    }
}
