import React from "react"

export default class TopBar extends React.Component {

    render() {
        return <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
            <a class="navbar-brand" href="#">Dashboard</a>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                </ul>
            </div>
        </nav>
    }
}

