import React from "react"
//import {changeTitle} from "../../../actions/headerActions";
import {connect} from "react-redux";
import {showFlash} from "../../action/headerActions";

@connect((store) => {
    return {
        header : store.header
    };
}, {showFlash})
export default class Index extends React.Component {

    componentDidMount() {
        this.props.showFlash('danger', "message 1");
        setTimeout( () => {
            this.props.showFlash('danger', "message 2");
        }, 2000);
    }

    render() {
        return <div></div>
    }
}
