import { combineReducers } from "redux"

import header from "./headerReducer";

export const rootReducer = combineReducers({
    header
});
