import {SHOW_FLASH, HIDE_FLASH} from "../ActionTypes";

export default function reducer(state={
    breadcrumb: [],
    title: null,
    titleDisplayed: false,
    flashes: [],
}, action) {

    switch (action.type) {
        //----------------> FETCH BRANDS
        case SHOW_FLASH: {
            return {...state, flashes: [...state.flashes, action.payload]}
        }
        case HIDE_FLASH: {
            let index = _.findIndex(state.flashes, {id: action.payload});
            let newFlashes =  [
                ...state.flashes.slice(0, index), // why isn't this working???
                ...state.flashes.slice(index + 1)
            ];
            console.log(newFlashes);

            return {...state, flashes: newFlashes}
        }
    }

    return state
}
