import {SHOW_FLASH, HIDE_FLASH} from "../ActionTypes";

export const showFlash = (type, message) => ({ type: SHOW_FLASH, payload: {id: 'flash-'+new Date().getTime(), type, message} });
export const hideFlash = (id) => ({ type: HIDE_FLASH, payload: id });