import {hideFlash} from "../../action/headerActions";
import {SHOW_FLASH} from "../../ActionTypes";

export const flashEpic = (action$) => action$
    .ofType(SHOW_FLASH)
    .delay(5000)
    .map((data)=> hideFlash(data.payload.id));