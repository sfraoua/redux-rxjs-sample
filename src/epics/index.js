import {combineEpics} from "redux-observable";
import {flashEpic} from "./header/flashEpic";

export const rootEpic = combineEpics(
    flashEpic
);